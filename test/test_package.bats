# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "test creating the python package" {
    create_template
    make -C ${PROJECT_FOLDER} dist
}

@test "test creating the python package (keywords)" {
    create_template '{keywords: "key1,key2"}'
    make -C ${PROJECT_FOLDER} dist
}
