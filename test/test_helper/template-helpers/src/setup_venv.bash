# setup_venv
# ==========
#
# Summary: Setup a virtual environment in the created test repo and activate
#
# Usage: setup_venv
#
# IO:
#   STDERR - the failed expression, on failure
# Globals:
#   none
# Returns:
#   0 - if the venv has been created and activated successfully
#   1 - otherwise
#
#   ```bash
#   @test 'venv()' {
#     setup_venv
#   }
#   ```
#

# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: MIT

setup_venv() {
  make -C ${PROJECT_FOLDER} venv-install
  source ${PROJECT_FOLDER}/venv/bin/activate
}
