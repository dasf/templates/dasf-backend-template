"""
ASGI config for testproject project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/asgi/
"""

import os

from channels.routing import ProtocolTypeRouter, URLRouter
from django.core.asgi import get_asgi_application
from django.urls import path

import dasf_broker.routing as dasf_routing
from dasf_broker.app_settings import DASF_WEBSOCKET_URL_ROUTE
from dasf_broker.token_auth_middleware import TokenAuthMiddlewareStack

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "testproject.settings")


application = ProtocolTypeRouter(
    {
        "http": get_asgi_application(),
        "websocket": TokenAuthMiddlewareStack(
            URLRouter(
                [
                    path(
                        DASF_WEBSOCKET_URL_ROUTE,
                        URLRouter(dasf_routing.websocket_urlpatterns),
                    )
                ]
            )
        ),
    }
)
