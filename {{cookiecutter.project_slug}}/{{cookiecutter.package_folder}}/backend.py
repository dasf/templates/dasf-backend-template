"""Backend module for {{ cookiecutter.project_slug }}."""

import os
from typing import Dict

from demessaging import configure, main, registry  # noqa: F401

__all__ = ["version_info"]


def version_info() -> Dict[str, str]:
    """Get the version of extpar and extpar_client."""
    import {{ cookiecutter.package_folder }}

    info = {
        "{{ cookiecutter.project_slug }}": {{ cookiecutter.package_folder }}.__version__,
    }
    return info


if __name__ == "__main__":
    if os.getenv("DE_MESSAGING_ENV_FILE"):
        from demessaging.config import BaseMessagingConfig, ModuleConfig

        config = ModuleConfig(
            messaging_config=BaseMessagingConfig(  # type: ignore[arg-type]
                topic="{{ cookiecutter.default_topic }}",
                _env_file=os.environ["DE_MESSAGING_ENV_FILE"],
            )
        )
        main(config=config)
    else:
        main(messaging_config=dict(topic="{{ cookiecutter.default_topic }}"))
