"""pytest configuration script for {{ cookiecutter.project_slug }}."""
from __future__ import annotations

{%- if cookiecutter.render_backend == "yes" %}
from typing import Callable, TYPE_CHECKING

import pytest

if TYPE_CHECKING:
    import subprocess as spr

    from dasf_broker.tests.live_ws_server_helper import ChannelsLiveServer


@pytest.fixture
def connected_module(
    db,
    monkeypatch,
    live_ws_server: ChannelsLiveServer,
    connect_module: Callable[[str, str], spr.Popen], random_topic: str
) -> spr.Popen:
    """A process that connects the backend module to the message broker.

    This fixtures uses the ``connect_module`` fixture of the
    ``dasf-broker-django`` package to connect the backend module at
    ``{{ cookiecutter.package_folder }}.backend`` to a live server that is
    started at localhost."""
    {%- if cookiecutter.render_api == "yes" %}
    from demessaging.config import WebsocketURLConfig

    from {{ cookiecutter.package_folder }} import api
    {%- endif %}

    process = connect_module(random_topic, "{{ cookiecutter.package_folder }}.backend")

    {%- if cookiecutter.render_api == "yes" %}

    config = WebsocketURLConfig(
        topic=random_topic, websocket_url=live_ws_server.ws_url + "/ws/"
    )
    monkeypatch.setattr(
        api.BackendModule.backend_config, "messaging_config", config
    )
    {%- endif %}

    return process

{%- endif %}
