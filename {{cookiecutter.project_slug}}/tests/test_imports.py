"""Test file for imports."""

def test_package_import():
    """Test the import of the main package."""
    import {{ cookiecutter.package_folder }}  # noqa: F401

{% if cookiecutter.render_backend == "yes" %}
def test_backend_import():
    import {{ cookiecutter.package_folder }}.backend  # noqa: F401

{% if cookiecutter.render_api == "yes" %}
def test_api_import():
    import {{ cookiecutter.package_folder }}.api  # noqa: F401

{% endif %}
{% endif %}
