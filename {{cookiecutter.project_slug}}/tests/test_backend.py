"""Test module for :mod:`{{ cookiecutter.package_folder}}.backend`."""


def test_show_version():
    from {{ cookiecutter.package_folder }} import __version__ as ref_version
    from {{ cookiecutter.package_folder }} import backend
    version_info = backend.version_info()
    assert "{{ cookiecutter.project_slug }}" in version_info
    assert version_info["{{ cookiecutter.project_slug }}"] == ref_version
